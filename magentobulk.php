<?php
set_time_limit(0);
ini_set('memory_limit', '1024M');
include_once "../app/Mage.php";
Mage::init();
 
if (($handle = fopen("Sku.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $num = count($data);   // you do NOT need this, this is for testing
        $row++;                      // you do NOT need this, this is for testing
        if($row == 0) continue; // you do NOT need this, this is for testing
 
        $old_sku = $data[0];
        $new_sku = str_replace('/', '-', $old_sku);
        echo $old_sku.'   change to  ->>>>>>>>>      '.$new_sku."\n";
        Mage::getModel('catalog/product')->loadByAttribute('sku', $old_sku)->setSku($new_sku)->save();
    }
    fclose($handle); 
}
